#include "jeu.h"
#include <stdlib.h>
#include <time.h>

char hasard() {
    srand(time(NULL));
    int choix = rand() % 3;
    if (choix == 0) return 'R';
    else if (choix == 1) return 'P';
    else return 'C';
}

char comparaison(char joueur, char ordinateur) {
    if (joueur == ordinateur) return 'E'; 
    else if ((joueur == 'R' && ordinateur == 'C') ||
             (joueur == 'P' && ordinateur == 'R') ||
             (joueur == 'C' && ordinateur == 'P'))
        return 'G'; 
    else
        return 'P'; 
}
