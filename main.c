#include "jeu.h"
#include <stdio.h>

int main() {
    char joueur, ordinateur;
    joueur = hasard();
    ordinateur = hasard();
    printf("Choix du joueur : %c\n", joueur);
    printf("Choix de l'ordinateur : %c\n", ordinateur);
    printf("Résultat : %c\n", comparaison(joueur, ordinateur));
    return 0;
}
